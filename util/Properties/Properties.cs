﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.AccessControl;
using System.IO;

namespace Bech.Util
{
    /// <summary>
    /// Advanced libary for making property files
    /// 
    /// A property file follows this pattern:
    /// #Comment
    /// propertyName = propertyValue
    /// </summary>
    public class Properties: IEnumerable<Property>
    {
        private string file;
        private Dictionary<string, Property> properties;
        private Dictionary<Property, string[]> comments;
        
        /// <summary>
        /// Constructs Properties on file in filePath
        /// Automatically loads when constructed
        /// </summary>
        /// <param name="filePath"></param>
        public Properties(string filePath)
        {
            file = filePath;
            properties = new Dictionary<string, Property>();
            comments = new Dictionary<Property, string[]>();
            Load();
        }

        /// <summary>
        /// Constructs the properties on file in filePath
        /// And sets the default properties from the defaultProperties
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="defaultProperties"></param>
        public Properties(string filePath, IEnumerable<Property> defaultProperties)
            : this(filePath)
        {
            foreach (Property p in defaultProperties)
            {
                SetDefault(p.Name, p.String);
            }
        }

        /// <summary>
        /// Load the properties from the specified file
        /// </summary>
        public void Load()
        {
            if (File.Exists(file))
            {
                readProperties(File.ReadAllLines(file));
            }
        }

        /// <summary>
        /// Save the properties to the specified file
        /// </summary>
        public void Save()
        {
            File.WriteAllLines(file,saveProperties());
        }

        /// <summary>
        /// Get the property with the name
        /// If the property does not exist it will be created with null as value
        /// </summary>
        /// <param name="name">Name of the property</param>
        /// <returns>Property with name</returns>
        public Property this[string name]
        {
            get {
                
                if (!properties.ContainsKey(name)){
                    properties[name] = new Property(name);
                }

                return properties[name];
            }
            set { properties[name] = value; }
        }
        
        /// <summary>
        /// Adds a property
        /// </summary>
        /// <param name="p">Property to be added</param>
        public void AddProperty(Property p)
        {
            this[p.Name] = p;
        }

        /// <summary>
        /// Sets the default value for the property with name
        /// </summary>
        /// <param name="name">Name to set default value of</param>
        /// <param name="value">Default value</param>
        public void SetDefault(string name, string value)
        {
            if (!properties.ContainsKey(name))
            {
                properties[name] = new Property(name,value);
            }
        }

        /// <summary>
        /// reads properties from string array list
        /// </summary>
        /// <param name="list">List of properties</param>
        private void readProperties(string[] list)
        {
            List<string> propertyComments = new List<string>(); //Comments for a property
            foreach (string line in list)
            {
                if (line.StartsWith("#")) //A comment have been found
                {
                    propertyComments.Add(line); //Add it to the comment list
                    continue;
                }
                string[] splitted = line.Split('='); //Split on =
                string name = splitted[0].Trim(); //No spaces
                string value = string.Join("=",splitted.Skip(1).ToArray()).Trim(); //To avoid problems if the value contains '='
                Property p = new Property(name, value); //The new property
                properties[name] = p; //Add the property
                if(propertyComments.Count>0){ //If there is any comment for the new property
                    comments[p] = propertyComments.ToArray(); //Save the properties comments
                    propertyComments = new List<string>(); //Perpare for the next property
                }
            }
        }

        /// <summary>
        /// saves the properties into a string array
        /// </summary>
        /// <returns>String array with all the properties</returns>
        private string[] saveProperties()
        {
            List<string> list = new List<String>();
            foreach(Property property in properties.Values){
                if (comments.ContainsKey(property))
                {
                    list.AddRange(comments[property]);
                }
                list.Add(string.Format("{0}={1}",property.Name,property.String));
            }
            return list.ToArray();
        }

        /// <summary>
        /// Gets the Enumerator for all the properties
        /// </summary>
        /// <returns>Enumerator</returns>
        public IEnumerator<Property> GetEnumerator()
        {
            return properties.Values.GetEnumerator();
        }

        /// <summary>
        /// Gets the Enumerator for all the properties
        /// </summary>
        /// <returns>Enumerator</returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return properties.Values.GetEnumerator();
        }
    }
}
