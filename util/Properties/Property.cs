﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bech.Util
{
    /// <summary>
    /// A single property
    /// Contaning its name and value
    /// 
    /// Value can be retrieved as the following types:
    /// - String
    /// - Boolean
    /// - Int
    /// - Double
    /// </summary>
    public class Property
    {
        private string name;
        private string value;
        
        /// <summary>
        /// Constructs a property with a name and a value
        /// </summary>
        /// <param name="name">Name of the property</param>
        /// <param name="value">Value of the property</param>
        public Property(string name, string value)
        {
            this.name = name;
            this.value = value;
        }

        /// <summary>
        /// Constructs a property but only with a name and with value as null
        /// </summary>
        /// <param name="name">Name of the property</param>
        public Property(string name)
        {
            this.name = name;
            value = null;
        }

        /// <summary>
        /// The value of this property as a string
        /// </summary>
        public string String
        {
            get{ return value.ToString();}
            set { this.value = value; }
        }

        /// <summary>
        /// The value of thie property as a boolean
        /// </summary>
        public bool Boolean
        {
            get{ return bool.Parse(value);}
            set { this.value = value.ToString(); }
        }

        /// <summary>
        /// The value of this property as an integer
        /// </summary>
        public int Integer
        {
            get { return int.Parse(value); }
            set { this.value = value.ToString(); }
        }

        /// <summary>
        /// The value of this property as a double
        /// </summary>
        public double Double
        {
            get { return double.Parse(value); }
            set { this.value = value.ToString(); }
        }

        /// <summary>
        /// The name of the property
        /// </summary>
        public string Name
        {
            get { return name; }
        }

        /// <summary>
        /// Is the value of this property null?
        /// </summary>
        /// <returns></returns>
        public bool IsNull()
        {
            return value == null;
        }
    }
}
